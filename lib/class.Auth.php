<?php

	/** Manages authentication */
	class Auth {
		
		private $filepath;
		
		private function __construct($filepath) {
			$this->filepath = $filepath;
		}
		
		/** Init authentication
		 * @throws AuthException If not authenticated
		 * @return Auth Authentication object */
		public static function init() {
			
			// If token is not given and service is auth, client is trying to authenticate
			if (empty($_REQUEST['token']) && $_REQUEST['service'] == 'auth') {
				
				// Check passphrase
				if (empty($_REQUEST['passphrase'])) throw new AuthException('Passphrase not given, cannot authenticate.');
				if (empty($_REQUEST['device'])) throw new AuthException('Device ID not given, cannot authenticate.');
				if ($_REQUEST['passphrase'] != $GLOBALS['config']['auth']['passphrase']) throw new AuthException('Wrong authentication passphrase.');
				
				// Passphrase is ok, create a random token file
				do {
					$token = strtolower(md5(mt_rand() . '@' . microtime()));
					$filepath = BERRY_BEE_ROOT . 'auth/' . $token . '.token';
				} while (file_exists($filepath));
				$ret = @file_put_contents($filepath, $_REQUEST['device'] . "\n");
				if ($ret === false) throw new AuthException('Cannot create authentication file.');
				
				// Give out token and instantiate the Auth object
				$GLOBALS['response']['token'] = $token;
				return new self($filepath);
				
			// Normal request (not trying to authenticate)
			} else {
				
				// Check token
				if (empty($_REQUEST['token'])) throw new AuthException('Auth token not given.');
				if (!preg_match('/^[0-9a-f]{32}$/i', $_REQUEST['token'])) throw new AuthException('Invalid auth token format.');
				
				// Lookup auth file
				$filepath = BERRY_BEE_ROOT . 'auth/' . strtolower($_REQUEST['token']) . '.token';
				if (!is_file($filepath)) throw new AuthException('Invalid auth token, you are not authenticated.');
				
				// Ok, instantiate the Auth object
				return new self($filepath);
				
			}
			
		}
		
		/** Log request to authentication file */
		public function logRequest() {
			$date = date('Y-m-d H:i:s');
			$ip = $_SERVER['REMOTE_ADDR'];
			$uri = $_SERVER['REQUEST_URI'];
			@file_put_contents($this->filepath, '[' . $date . '] ' . $ip . ' ' . $uri . "\n", FILE_APPEND);
			return $this;
		}
		
	}

	class AuthException extends Exception { }

?>