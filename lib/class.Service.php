<?php
	
	/** Service dummy class.
	 * Extend this class in /services folder to make new service classes.
	 * Constructor should have one parameter: service configuration array. */
	class Service {
		
		/** Instantiate a new service from configuration */
		public static function instantiate($serviceName) {
			
			// If service name is empty or "auth" do nothing
			if (empty($serviceName) || ($serviceName == 'auth')) return new self();
			
			// Load service definition from configuration
			if (empty($GLOBALS['config']['services'][$serviceName])) throw new ServiceException('Non existent service.');
			$service = $GLOBALS['config']['services'][$serviceName];
			
			// Make and return instance
			$serviceClass = $service['class'];
			$classPath = BERRY_BEE_ROOT . 'services/class.' . $serviceClass . '.php';
			if (!file_exists($classPath)) throw new ServiceException('Cannot instantiate service class.');
			return new $serviceClass($service['configuration']);
			
		}
		
		/** Return a user-friendly services list */
		public static function listServices() {
			$list = array();
			foreach ($GLOBALS['config']['services'] as $name => $definition) $list[] = array(
				'name' => $name, 
				'title' => $definition['title'],
				'description' => $definition['description'],
				'class' => $definition['class']
			);
			return $list;
		}
		
		/** Main service method */
		public function run() {
			$GLOBALS['response']['name'] = $GLOBALS['config']['name'];
			$GLOBALS['response']['description'] = self::description(); 
			$GLOBALS['response']['services'] = self::listServices();
		}
		
		/** Returns passed value, use this function to wrap configuration values
		 * @param String $value The value, will be returned as it is
		 * @param String $default If not NULL represent the default value in case value is empty
		 * @throws ServiceConfigurationException When value is empty and default is not given */
		public static function config($value, $default = null) {
			if (empty($value) && $value !== 0 && $value !== false && $value !== '0') $value = $default;
			if (is_null($value)) throw new ServiceConfigurationException('Service is not fully configured.');
			return $value;
		}
		
		/** Returns folder path from main Berry bee configuration, with no slash at the end
		 * @throws FolderException When folder is not declared, not exists or it's not accessible */
		public static function folder($name, $checkFolder = true) {
			$path = $GLOBALS['config']['folders'][$name];
			if (empty($path)) throw new FolderException('Folder "' . $name . '" is not declared.');
			if ($checkFolder && !is_dir($path)) throw new FolderException('Folder "' . $name . '" does not exist.');
			return (substr($path, -1) == '/' ? substr($path, 0, -1) : $path);
		}
		
		/** Returns the server description from main Berry bee configuration, that is the "description"
		 * string and/or the output of the descriptionCommand command (if set) */
		public static function description() {
			$d = "";
			if (!empty($GLOBALS['config']['description'])) $d .= $GLOBALS['config']['description'] . "\n";
			if (!empty($GLOBALS['config']['descriptionCommand'])) $d .= shell_exec($GLOBALS['config']['descriptionCommand']);
			return trim($d);
		}
		
	}
	
	class ServiceException extends Exception { }
	class ServiceConfigurationException extends ServiceException { }
	class FolderException extends ServiceException { }
	
?>
