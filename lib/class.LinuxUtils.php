<?php
	
	/** Utility class for Linux shell commands wrapping */
	class LinuxUtils {
		
		/** Run command on Linux shell and returns he whole output as string
		 * @param String $command Command line string (will not be escaped!)
		 * @param String $args An arbitrary number of arguments that will be escaped and appended to command line */
		public static function exec($command, $args = null) {
			if (empty($command)) return '';
			ob_start();
			$args = array_slice(func_get_args(), 1);
			$escapedArgs = array_map(function($a) { return escapeshellarg($a); }, $args);
			@passthru($command . ' ' . implode(' ', $escapedArgs));
			return ob_get_clean();
		}
		
		/** Run command on Linux shell and returns the whole output as string
		 * @param String $command Command line string (will not be escaped!)
		 * @param String $retVal If present, the return status of the Unix command will be placed here.
		 * @param String $args... An arbitrary number of arguments that will be escaped and appended to command line */
		public static function execRetVal($command, &$retVal = null, $args = null) {
			if (empty($command)) return '';
			ob_start();
			$args = array_slice(func_get_args(), 2);
			$escapedArgs = array_map(function($a) { return escapeshellarg($a); }, $args);
			@passthru($command . ' ' . implode(' ', $escapedArgs), $retVal);
			return ob_get_clean();
		}
		
		/** Given a multiline $text, returns an array containing lines that matches regular expression $pattern */
		public static function grep($text, $pattern, $returnAsString = false) {
			if (empty($text)) return array();
			$grep = array();
			foreach (explode("\n", $text) as $line) if (preg_match($pattern, trim($line))) $grep[] = trim($line);
			return ($returnAsString ? implode("\n", $grep) : $grep);
		}
		
		/** Get the list of files in a folder
		 * @param String $path Folder path
		 * @param String $regex A regular expression: if specified, returns only filenames matching it
		 * @return Array The list of full paths of files */
		public static function ls($path, $regex = null) {
			$ls = array();
			if (($dh = @opendir($path)) === false) return $ls;
			if (substr($path, -1) != '/') $path .= '/';
			while (($file = readdir($dh)) !== false) {
				if (substr($file, 0, 1) == '.') continue;
				if (!is_null($regex)) if (!preg_match($regex, $file)) continue;
				$ls[] = $path . $file;
			}
			return $ls;
		}
		
	}

?>
