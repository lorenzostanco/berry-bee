<?php

	// Define Berry bee root path
	define('BERRY_BEE_ROOT', dirname(__FILE__) . '/');
	
	// Fix inputs
	foreach ($_REQUEST as $k => $v) $_REQUEST[$k] = trim($v);
	
	// Classes autoload
	spl_autoload_register(function($class) {
		$classPath = BERRY_BEE_ROOT . 'lib/class.' . $class . '.php';
		if (file_exists($classPath)) { require_once $classPath; return; }
		$classPath = BERRY_BEE_ROOT . 'services/class.' . $class . '.php';
		if (file_exists($classPath)) { require_once $classPath; return; }
	});
	
	// Suppress "Undefined index" errors 
	error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
	
	// Locale
	setlocale(LC_ALL, "en_US.UTF-8");
	
	// Build response
	$response = array('error' => false);
	try {
		
		// Load configuration
		$configNeon = @file_get_contents(BERRY_BEE_ROOT . 'configuration.neon');
		if (empty($configNeon)) throw new Exception('Cannot load Berry bee configuration, file configuration.neon does not exist.');
		$config = Neon::decode($configNeon);
		if (empty($config)) throw new Exception('Cannot load Berry bee configuration.');
	
		// Authenticate
		Auth::init()->logRequest();
		
		// Run service
		Service::instantiate($_REQUEST['service'])->run();
		
	// Manage errors
	} catch (Exception $x) {
		$response = array(
			'error' => true,
			'errorType' => get_class($x),
			'errorMessage' => $x->getMessage()
		);
	}
	
	// Output response
	header('Content-Type: application/json; charset=utf-8');
	echo json_encode($response, JSON_PARTIAL_OUTPUT_ON_ERROR);
	
?>
