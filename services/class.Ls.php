<?php

	/**
	 * List files in a directory and allows operations such as download, copy, move and remove.
	 * 
	 * Configuration:
	 *  - folder: name of target folder, must be one of the names defined in the "folder" section of the main Berry bee configuration
	 *  - regex: a regular expression string, if given only filenames matching it will be listed
	 *  - download: TRUE if you want to allow user to remotely download files from this directory (default: TRUE)
	 *  - rm: TRUE if you want to allow user to delete files in this directory (default: FALSE)
	 *  - move: if TRUE user can move files in subdirectories (default: FALSE)
	 *  - rename: if TRUE user can rename files and subdirectories (default: FALSE)
	 *  - mkdir: if TRUE user can create new subdirectories (default: FALSE)
	 *  - copyto: an array of names of other Ls service instances, files here can be copied there (default: none)
	 *  - browse: allows subdirectories browsing (see below) (default: FALSE)
	 *  - mime_cache: if TRUE, caches mime to disk for the future (cache key is path and mtime), recommended for folders containing large files (default: FALSE)
	 *  - mime_fast: retrieve mime type and charset using file extension (dafault: FALSE)
	 *  - no_filesize: makes the service faster by avoiding stat every file, filesize will always be zero (default: FALSE)
	 *
	 * Additional request parameters:
	 *  - browse: A name of one or many nested subdirectories (for example "subdir" or "subdir/secondlevel")
	 *     This parameter allows subdirectories browsing: files list and commands will be relative to a subdirectory instead of the main folder.
	 *     It works only if "browse" configuration paramter is TRUE. Climbing up ("../../hack") is forbidden.
	 * 
	 * Commands:
	 *  - download: Starts file downloading
	 *     - filename: Name of the file to to download (name only, not full path)
	 *  - rm: Remove a file
	 *     - filename: Name of the file to remove (name only, not full path)
	 *  - move: Moves a file in a subdirectory
	 *     - filename: Name of the file to move
	 *     - sub: Name of the subdirectory in which file should be moved
	 *  - rename: Rename a file or a subdirectory
	 *     - old: Original file/subdirectory name
	 *     - new: New file/subdirectory name
	 *  - mkdir: Make a new subdirectory
	 *     - sub: Name of the new subdirectory
	 *  - copy: Starts copying a file from another directory into here
	 *     - folder: Name of the source folder, must be one of the names defined in the "folder" section of the main Berry bee configuration
	 *     - sourcebrowse: the "browse" parameter if the source file is in a subdirectory of the source folder
	 *     - filename: Name of the source file
	 * 
	 * Response:
	 *  - folder: folder name, one of the names defined in the "folder" section of the main Berry bee configuration
	 *  - path: folder complete path
	 *  - download: TRUE or FALSE depending on if this instance allows files downloading
	 *  - rm: TRUE or FALSE depending on if this instance allows files removing
	 *  - move: TRUE or FALSE depending on if this instance allows files to be moved into subdirectories
	 *  - rename: TRUE or FALSE depending on if this instance allows files and subdirectories to be renamed
	 *  - mkdir: TRUE or FALSE depending on if this instance allows subdirectories creation
	 *  - copyto: object, names and titles of service instances in which files can be copied to
	 *  - browse; TRUE or FALSE depending on if this istance allows subdirectories browsing
	 *  - subs: an array of objects, representing subdirectories
	 *     - name: folder name
	 *     - mtime: modification time, as UNIX timestamp
	 *  - files: an array of objects, representing files in this directory
	 *     - name: filename as string
	 *     - size: filesize in bytes, as string, "0" if "no_filesize"
	 *     - copying: if non-zero, this file is a destination of a copy process and that number represent 
	 *                source file size (should be compared to "size" number to get progress!)
	 *     - mtime: file modification time, as UNIX timestamp
	 *     - mime: file MIME (type and charset)
	 */
	class Ls extends Service {
		
		protected $folder;
		protected $path; // Current browsing path
		protected $path_base; // Reference to first level path (i.e. with no browsing path)
		protected $regex = null;
		protected $download = true;
		protected $rm = false;
		protected $move = false;
		protected $rename = false;
		protected $mkdir = false;	
		protected $copyto = array();
		protected $browse = false;
		protected $mime_cache = false;
		protected $mime_cache_content = null;
		protected $mime_cache_filepath = null;
		protected $mime_fast = false;
		protected $no_filesize = false;
		
		public function __construct($configuration, $checkFolder = true) {
			
			// Basic configuration
			$this->folder = $configuration['folder'];
			$this->path = Service::folder(Service::config($configuration['folder']), $checkFolder);
			$this->path_base = $this->path;
			$this->regex = empty($configuration['regex']) ? null : $configuration['regex']; 
			$this->download = Service::config($configuration['download'], true);
			$this->rm = Service::config($configuration['rm'], false);
			$this->move = Service::config($configuration['move'], false);
			$this->rename = Service::config($configuration['rename'], false);
			$this->mkdir = Service::config($configuration['mkdir'], false);
			$this->browse = Service::config($configuration['browse'], false);
			$this->mime_cache = Service::config($configuration['mime_cache'], false);
			$this->mime_fast = Service::config($configuration['mime_fast'], false);
			$this->no_filesize = Service::config($configuration['no_filesize'], false);
			
			// Create the "copyto" array, with service names as key and title as value
			if (is_array($configuration['copyto'])) foreach ($configuration['copyto'] as $serviceName) {
				if (empty($GLOBALS['config']['services'][$serviceName])) continue;
				$this->copyto[$serviceName] = $GLOBALS['config']['services'][$serviceName]['title'];
			}

			// Browsing?
			$browse = self::sanitizeBrowse($_REQUEST['browse']);
			if (!empty($browse)) {
				if (!$this->browse) throw new CannotBrowseException('You cannot browse subdirectories in this directory.');
				$newPath = $this->path . '/' . $browse;
				if (!is_dir($newPath)) throw new FolderException('Cannot browse "' . $browse . '", directory does not exist.');
				$this->path = $newPath;
			}

			// MIME cache filepath
			if ($this->mime_cache) {
				$this->mime_cache_filepath = BERRY_BEE_ROOT . 'cache/mime-' . self::sanitizeFilename($_REQUEST['service']) . '.pser';
			}
			
		}
		
		public function run() {
			
			// Give service instance info
			$GLOBALS['response']['folder'] = $this->folder;
			$GLOBALS['response']['path'] = $this->path;
			$GLOBALS['response']['download'] = $this->download;
			$GLOBALS['response']['rm'] = $this->rm;
			$GLOBALS['response']['move'] = $this->move;
			$GLOBALS['response']['rename'] = $this->rename;
			$GLOBALS['response']['mkdir'] = $this->mkdir;
			$GLOBALS['response']['copyto'] = $this->copyto;
			$GLOBALS['response']['browse'] = $this->browse;
			
			// Parse commands
			switch ($_REQUEST['command']) {
				
				// Download
				case 'download':
					if ($this->download) {
						$filename = self::sanitizeFilename($_REQUEST['filename']);
						if (empty($filename)) throw new CannotDownloadException('Specify the name of the file you want to download.');
						self::download($filename);
					} else {
						throw new CannotDownloadException('You cannot download files from this directory.');
					}
					break;
				
				// Remove
				case 'rm':
					if ($this->rm) {
						$filename = self::sanitizeFilename($_REQUEST['filename']);
						if (empty($filename)) throw new CannotRemoveException('Specify the name of the file you want to remove.');
						if (!is_file($this->path . '/' . $filename)) throw new CannotRemoveException('Specified file does not exists.');
						if (!@unlink($this->path . '/' . $filename)) throw new CannotRemoveException('Cannot remove file "' . $filename . '".');
					} else {
						throw new CannotRemoveException('You cannot remove files in this directory.');
					}
					break;
					
				// Move
				case 'move':
					if ($this->move) {
						$filename = self::sanitizeFilename($_REQUEST['filename']);
						$subdirectory = self::sanitizeBrowse(self::sanitizeFilename($_REQUEST['sub']));
						if (empty($filename)) throw new CannotMoveException('Specify the name of the file you want to move.');
						if (empty($subdirectory)) throw new CannotMoveException('Specify the name of the subdirectory in which you want to move the file.');
						if (!is_file($this->path . '/' . $filename)) throw new CannotMoveException('Specified file does not exists.');
						if (!is_dir($this->path . '/' . $subdirectory)) throw new CannotMoveException('Specified subdirectory does not exists.');
						if (file_exists($this->path . '/' . $subdirectory . '/' . $filename)) throw new CannotMoveException('A file or directory with the same name already exists in "' . $subdirectory . '".');
						if (!@rename($this->path . '/' . $filename, $this->path . '/' . $subdirectory . '/' . $filename)) throw new CannotMoveException('Cannot move file "' . $filename . '" in "' . $subdirectory . '".');
					} else {
						throw new CannotMoveException('You cannot move files into subdirectories.');
					}
					break;

				// Rename
				case 'rename':
					if ($this->rename) {
						$old = self::sanitizeBrowse(self::sanitizeFilename($_REQUEST['old']));
						$new = self::sanitizeBrowse(self::sanitizeFilename($_REQUEST['new']));
						if (empty($old)) throw new CannotRenameException('Specify the name of the file you want to rename.');
						if (empty($new)) throw new CannotRenameException('Specify the new name of the file.');
						if (!file_exists($this->path . '/' . $old)) throw new CannotRenameException('Specified file does not exists.');
						if (file_exists($this->path . '/' . $new)) throw new CannotRenameException('A file or directory with the same name already exists.');
						if (!@rename($this->path . '/' . $old, $this->path . '/' . $new)) throw new CannotRenameException('Cannot rename file "' . $old . '" in "' . $new . '".');
					} else {
						throw new CannotRenameException('You cannot rename files here.');
					}
					break;
					
				// Make dir
				case 'mkdir':
					if ($this->mkdir) {
						$sub = self::sanitizeBrowse(self::sanitizeFilename($_REQUEST['sub']));
						if (empty($sub)) throw new CannotMkdirException('Specify the name of the subdirectory you want to create.');
						if (file_exists($this->path . '/' . $sub)) throw new CannotMkdirException('A file or directory with the same name already exists.');
						if (!@mkdir($this->path . '/' . $sub, 0777)) throw new CannotMkdirException('Cannot create directory "' . $sub . '".');
						@chmod($this->path . '/' . $sub, 0777);
					} else {
						throw new CannotMkdirException('You cannot create subdirectories here.');
					}
					break;
					
				// Copy (all parsing and checks are done in copy() method)
				case 'copy':	
					$this->copy($_REQUEST['folder'], $_REQUEST['filename']);
					break;
				
			}
			
			// List files
			$GLOBALS['response']['subs'] = array();
			$GLOBALS['response']['files'] = array();
			$ls = LinuxUtils::ls($this->path, $this->regex);
			natcasesort($ls);
			foreach ($ls as $filepath) {
				if (is_dir($filepath)) {
					$GLOBALS['response']['subs'][] = array(
						'name' => basename($filepath),
						'mtime' => filemtime($filepath)
					); 
				} else {
					
					// Get filename and filesize
					$filename = basename($filepath);
					$filesize = $this->no_filesize ? 0 : trim(LinuxUtils::exec('stat -c %s', $filepath));
					if (empty($filesize)) $filesize = (string)"0";
					
					// Is a "copy in progress"?
					$copying = (string)"0";
					$copyingFilepath = $this->path . '/.berry-bee-copy-' . $filename;
					if (!$this->no_filesize && is_file($copyingFilepath)) {
						$copying = trim(@file_get_contents($copyingFilepath));
						if ($copying === $filesize) @unlink($copyingFilepath);
					}
					
					// Check problematic encoding for JSON
					$jsonProblematicEncoding = false;
					if (empty(@json_encode($filename))) {
						$jsonProblematicEncoding = true;
						$filenameUTF8 = iconv("windows-1251", "UTF-8", $filename);
					}
					
					$GLOBALS['response']['files'][] = array(
						'name' => $jsonProblematicEncoding ? $filenameUTF8 : $filename,
						'size' => $filesize,
						'copying' => ($copying === $filesize ? (string)"0" : (string)$copying),
						'mtime' => filemtime($filepath),
						'mime' => $this->getMime($filepath)
					);
					
				}
			}

			// Store MIME cache
			if ($this->mime_cache) {
				$new_mime_cache = array();

				// Add listed files to cache
				foreach ($ls as $filepath) {			
					if (!empty($this->mime_cache_content[$filepath])) {
						$new_mime_cache[$filepath] = $this->mime_cache_content[$filepath];
					}
				}

				// Keep in cache files from other foders
				if (!empty($this->mime_cache_content)) foreach ($this->mime_cache_content as $filepath => $_) {
					if (dirname($filepath) != $this->path) {
						$new_mime_cache[$filepath] = $this->mime_cache_content[$filepath];
					}
				}

				@file_put_contents($this->mime_cache_filepath, serialize($new_mime_cache));
			}
			
		}
		
		/** Start a download.
		 * @param String $filename File name
		 * @param Bool $resumeable If the download had to be resumeable, default TRUE
		 * @param String $mimetype Download MIME, default to 'application/octet-stream'
		 * @throws DownloadException If file does not exists */
		public function download($filename, $resumeable = true) {

			// Check file
			$filepath = $this->path . '/' . $filename;
			if (!is_file($filepath) || !is_readable($filepath)) throw new CannotDownloadException('Cannot start download, file does not exists or it\'s not readable.');

			// Setup size and name
			$filesize = filesize($filepath);
			$filename = basename($filepath);

			// Turn off output buffering to decrease CPU usage
			@ob_end_clean();

			// Required for IE, otherwise Content-Disposition may be ignored
			if (ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
			
			// First headers
			header('Content-Type: ' . $this->getMime($filepath, false, true));
			header('Content-Disposition: attachment; filename="' . $filename . '"');
			header('Content-Transfer-Encoding: binary');
			header('Accept-Ranges: bytes');
			header('Cache-control: private');
			header('Pragma: private');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

			// Multipart-download and download resuming support
			if ($resumeable && isset($_SERVER['HTTP_RANGE'])) {
				list($a, $range) = explode("=", $_SERVER['HTTP_RANGE'], 2);
				list($range) = explode(",", $range, 2);
				list($range, $range_end) = explode("-", $range);
				$range = intval($range);
				$range_end = ((!$range_end) ? ($filesize - 1) : intval($range_end));
				$new_length = $range_end - $range + 1;
				header("HTTP/1.1 206 Partial Content");
				header("Content-Length: $new_length");
				header("Content-Range: bytes $range-$range_end/$filesize");
			} else {
				$new_length = $filesize;
				header("Content-Length: " . $filesize);
			}

			/* Output the file itself */
			$chunksize = 1024 * 1024;
			$bytes_send = 0;
			$file = fopen($filepath, 'r');
			if ($file) {
				if ($resumeable && isset($_SERVER['HTTP_RANGE'])) fseek($file, $range);
				while (!feof($file) && (!connection_aborted()) && ($bytes_send < $new_length)) {
					$buffer = fread($file, $chunksize);
					print($buffer);
					flush();
					$bytes_send += strlen($buffer);
				}
				fclose($file);
			}

			// Done
			exit();

		}
		
		/** Copy a file in this directory
		 * @param $folder Source file folder name, must be one of the names defined in the "folder" section of the main Berry bee configuration
		 * @param $filename Source file name
		 * @throws CannotCopyException On errors */
		protected function copy($folder, $filename) {
			
			// Validate folder and filename
			$filename = self::sanitizeFilename($filename);
			if (empty($folder)) throw new CannotCopyException('Specify the folder from which you want to copy the file.');
			if (empty($filename)) throw new CannotCopyException('Specify the name of the file you want to copy.');
			$sourceBrowse = self::sanitizeBrowse($_REQUEST['sourcebrowse']);
			$source = Service::folder($folder) . '/' . (empty($sourceBrowse) ? '' : $sourceBrowse . '/') . $filename;
			if (!is_file($source)) throw new CannotCopyException('Source file "' . $filename . '" does not exist. Cannot copy.');
			if (file_exists($this->path . '/' . $filename)) throw new CannotCopyException('A file or directory with the same name already exists.');
			
			// Create an empty destination file
			if (@file_put_contents($this->path . '/' . $filename, '') === false) throw new CannotCopyException('Cannot write file "' . $filename . '". Cannot copy.');
			
			// Save the source file size in a hidden file
			if (@file_put_contents($this->path . '/.berry-bee-copy-' . $filename, trim(LinuxUtils::exec('stat -c %s', $source))) === false) throw new CannotCopyException('Cannot write file "' . $filename . '". Cannot copy.');
			
			// Start copy in background
			$cmd  = 'cp ' . escapeshellarg($source) . ' ' . escapeshellarg($this->path . '/' . $filename) . '; ';
			$cmd .= 'chmod uga+rw ' . escapeshellarg($this->path . '/' . $filename) . '; ';
			$cmd .= 'rm ' . escapeshellarg($this->path . '/.berry-bee-copy-' . $filename); // Remove hidden file
			@exec('( ' . $cmd . ' ) > /dev/null &');
			
		}
		
		/** Removes slashes from a string */
		protected static function sanitizeFilename($filename) {
			return str_replace('/', '', $filename);
		}
		
		/** Removes double dots (avoid going up directories tree) and trim slashes */
		protected static function sanitizeBrowse($path) {
			$path = str_replace('..', '', $path);
			return trim($path, '/');
		}
		
		/** Detect file MIME type */
		protected function getMime($filepath, $skipCache = false, $noFast = false) {
		
			// Fast MIME
			if ($this->mime_fast && !$noFast) {
				return FastMime::getMime($filepath);
	
			// Cache lookup if caching is enabled
			} elseif ($this->mime_cache && !$skipCache) {

				// Load cache file
				if (is_null($this->mime_cache_content)) {
					$this->mime_cache_content = @unserialize(file_get_contents($this->mime_cache_filepath));
					if (empty($this->mime_cache_content)) {
						$this->mime_cache_content = array(); // Empty cache on errors reading cache file
					}
				}

				// Lookup and filling
				$filemtime = filemtime($filepath);
				if (empty($this->mime_cache_content[$filepath]) || $this->mime_cache_content[$filepath]['mtime'] != $filemtime) {
					$this->mime_cache_content[$filepath] = array(
						'mime' => $this->getMime($filepath, true, true), // Get MIME skipping cache
						'mtime' => $filemtime
					);
				}
				return $this->mime_cache_content[$filepath]['mime'];
				
			// Caching not enabled
			} else {
				$finfo = finfo_open(FILEINFO_MIME);
				$mimetype = finfo_file($finfo, $filepath);
				finfo_close($finfo);
				return $mimetype;
			}

		}

	}
	
	class LsException extends ServiceException { }
	class CannotDownloadException extends LsException { }
	class CannotRemoveException extends LsException { }
	class CannotMoveException extends LsException { }
	class CannotRenameException extends LsException { }
	class CannotMkdirException extends LsException { }
	class CannotCopyException extends LsException { }
	class CannotBrowseException extends LsException { }
	
?>
