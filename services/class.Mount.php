<?php

	/**
	 * Extends Ls service implementing commands for mount and unmount devices. 
	 * 
	 * This service is intented to use with removable devices such as USB sticks.
	 * Normally only root user can (u)mount these devices. To bypass this 
	 * restriction you should use p(u)mount (http://packages.debian.org/it/sid/pmount)
	 * insteand of (u)mount. You can, in fact, specify any custom (u)mount command
	 * in this service configuration.
	 * Additionally, in Debian, the permission to execute pmount is restricted to 
	 * members of the system group plugdev. So, `sudo adduser www-data plugdev`
	 * should be run once on the Raspberry: this way the Apache user will be able 
	 * to use pmount. Apache restart or system reboot is necessary.
	 * 
	 * Configuration:
	 *  - folder: name of target mount point, same as "folder" for Ls services
	 *  - diskfilter: filter string to get the device disk object (optional, if not given the mount point will be used)
	 *  - mountcommand: command that mounts the device, you can leave empty if system has auto-mounting
	 *  - umountcommand: command that unmounts the device
	 *  - [all other Ls options]
	 * 
	 * Commands:
	 *  - mount: mount the device
	 *  - umount: unmount the device
	 *  - [all other Ls commands, they work only if device is mounted]
	 * 
	 * Response:
	 *  - mounted: TRUE if device is mounted, FALSE if not
	 *  - mountable: TRUE if "mountcommand" configuration parameter is given (not empty)
	 *  - umountable: TRUE if "umountcommand" configuration parameter is given (not empty)
	 *  - umountcommand: The given "umountcommand" configuration parameter
	 *  - disk: object, only if device is mounted
	 *     - filter: filter string for this disk
	 *     - mountpoint: actual mountpoint for this disk
	 *     - used: kbytes used
	 *     - total: total kbytes
	 *     - type: filesystem type, as string (for example "ext4")
	 *  - [all other Ls responses when device is mounted]
	 */
	class Mount extends Ls {
		
		protected $diskfilter;
		protected $mountcommand = "";
		protected $umountcommand = "";
		protected $mounted = false;
		
		public function __construct($configuration) {
			
			// (U)Mount commands
			$this->mountcommand = Service::config($configuration['mountcommand'], "");
			$this->umountcommand = Service::config($configuration['umountcommand'], "");
			
			// Ls configuration (do not check if folder exists)
			parent::__construct($configuration, false);
			
			// Disk filter configuration, default to base path			
			$this->diskfilter = Service::config($configuration['diskfilter'], $this->path_base);

		}
		
		public function run() {
			
			// (U)Mount
			if ($_REQUEST['command'] == 'mount') {
				if ($this->mountcommand !== "") LinuxUtils::exec($this->mountcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'umount') {
				if ($this->umountcommand !== "") LinuxUtils::exec($this->umountcommand);
				$_REQUEST['command'] = null;
			}

			// (U)Mountable? Let us say it
			$GLOBALS['response']['mountable'] = ($this->mountcommand !== "");
			$GLOBALS['response']['umountable'] = ($this->umountcommand !== "");
			
			// Determine if device is mounted, while getting disk usage
			$diskUsage = System::getDiskUsage($this->diskfilter);
			if (!empty($diskUsage)) {
				$this->mounted = true;
				$GLOBALS['response']['mounted'] = true;
				$GLOBALS['response']['disk'] = array(
					'filter' => $this->diskfilter,
					'mountpoint' => $diskUsage[3],
					'used' => $diskUsage[0],
					'total' => $diskUsage[1],
					'type' => $diskUsage[2]
				);
			} else {
				$this->mounted = false;
				$GLOBALS['response']['mounted'] = false;
			}
			
			// If device is not mounted we cannot run commands
			if (!$this->mounted && !empty($_REQUEST['command'])) throw new MountException('Cannot execute command. Device is not mounted.');
			
			// Run Ls service
			parent::run();
			
		}
		
	}
	
	class MountException extends LsException { }
	
?>
