<?php
	
	/** 
	 * Gives system informations
	 * 
	 * Configuration:
	 *  - disks: an array of filter strings i.e. the names of mount points you want to get space usage info 
	 *  - processes: number of most CPU stressing processes to return (default 10)
	 *  - customcommands: an array of objects representing custom commands and labels (optional), each object in the array is:
	 *     - label: the name of the command (for example "Date and time")
	 *     - command: the command to run (for example "date -R")
	 * 
	 * Response:
	 *  - uptime: number of seconds since the system is running (not returned if /proc/uptime not available)
	 *  - loadavg: an array of three floats representing system load for 1, 5 and 15 minutes (not returned if "top" not available)
	 *  - memory: an object with RAM info (not returned if "top" not available)
	 *     - used: kbytes used
	 *     - total total kbytes
	 *  - cpu: percentage (integer) representing CPU usage (not returned if "top" not available)
	 *  - processes: an array of object representing top processes (not returned if "top" not available)
	 *     - cpu: percentage (integer) representing CPU usage
	 *     - memory: percentage (float) representing memory usage
	 *     - command: command string
	 *  - disks: an array of objects, one for each filter given in configuration
	 *     - filter: filter string for this disk
	 *     - mountpoint: actual mountpoint for this disk
	 *     - used: kbytes used
	 *     - total: total kbytes
	 *     - type: filesystem type, as string (for example "ext4")
	 *  - customcommands: an array of objects representing the output from the "customcommands"
	 *     - label: name of the command, as given in configuration
	 *     - output: complete output of the command
	 */
	class System extends Service {
		
		protected $disks = array();
		protected $processes = 10;
		protected $customcommands = array();
		
		public function __construct($configuration) {
			if (!empty($configuration['disks']) && is_array($configuration['disks'])) $this->disks = $configuration['disks'];
			$this->processes = Service::config($configuration['processes'], 10);
			$this->customcommands = Service::config($configuration['customcommands'], array());
		}
		
		public function run() {
			
			// Get uptime
			$uptime = intval(LinuxUtils::exec('cat /proc/uptime'));
			if (!empty($uptime)) $GLOBALS['response']['uptime'] = $uptime;
			
			// Get "top" output
			$top = LinuxUtils::exec('top -bn1');
			
			// Read load average
			if (!empty($top)) if (preg_match('/^top - .+\\hload average:\\h+([0-9.]+),\\h+([0-9.]+),\\h+([0-9.]+)\\h*$/m', $top, $matches)) {
				$GLOBALS['response']['loadavg'] = array(floatval($matches[1]), floatval($matches[2]), floatval($matches[3]));
			}
			
			// Read memory usage
			if (!empty($top)) if (preg_match('/^(KiB\\h)?Mem:\\h*([0-9]+)k?\\htotal,\\h*([0-9]+)k?\\hused,/m', $top, $matches)) {
				$GLOBALS['response']['memory'] = array('used' => intval($matches[3]), 'total' => intval($matches[2]));
			}
			
			// Compute CPU usage and list top processes
			if (!empty($top)) if (preg_match_all('/^\\h*[0-9]+\\h+([^\\h]+\\h+){7}([0-9.]+)\\h+([0-9.]+)\\h+[0-9:.]+\\h+(.*)$/m', $top, $matches)) {
				$cpu = 0.0;
				$processes = array();
				for ($i = 0; $i < count($matches[0]); $i++) {
					$processCpu = floatval($matches[2][$i]);
					$cpu += (float)$processCpu;
					$processes[] = array(
						'cpu' => (float)$processCpu,
						'memory' => floatval($matches[3][$i]),
						'command' => trim($matches[4][$i])
					);
				}
				usort($processes, function($a, $b) { return (($b['cpu'] * 100.0 + $b['memory']) - ($a['cpu'] * 100.0 + $a['memory'])) * 100.0; });
				$GLOBALS['response']['cpu'] = min(100, $cpu);
				$GLOBALS['response']['processes'] = array_slice($processes, 0, $this->processes);
			}
			
			// Get disks usage
			$GLOBALS['response']['disks'] = array();
			foreach ($this->disks as $filter) {
				$diskUsage = self::getDiskUsage($filter);
				if (!empty($diskUsage)) $GLOBALS['response']['disks'][] = array(
					'filter' => $filter,
					'mountpoint' => $diskUsage[3],
					'used' => $diskUsage[0],
					'total' => $diskUsage[1],
					'type' => $diskUsage[2]
				);
			}
			
			// Custom commands
			$GLOBALS['response']['customcommands'] = array();
			foreach ($this->customcommands as $customcommand) {
				$GLOBALS['response']['customcommands'][] = array(
					'label' => $customcommand['label'],
					'output' => trim(LinuxUtils::exec($customcommand['command']))
				);
			}
			
		}
		
		/** Executes 'df -lT' and return used and total size (and filesystem type) of entry that contains $filter string */
		public static function getDiskUsage($filter) {
			$df = LinuxUtils::grep(LinuxUtils::exec('df -T'), '/' . preg_quote($filter, '/') . '/');
			if (empty($df)) return null;
			$matches = array();
			if (preg_match('/^[^\\h]+\\h+([^\\h]+)\\h+([0-9]+)\\h+([0-9]+)\\h+([0-9]+)\\h+[0-9]+%\\h+(.*)$/', $df[0], $matches)) {
				
				// Return used kbytes as total minus available kbytes (http://is.gd/aoG8PC)
				return array(intval($matches[2]) - intval($matches[4]), intval($matches[2]), $matches[1], trim($matches[5]));
				
			} else {
				return null;
			}
		}
		
	}
	

?>
