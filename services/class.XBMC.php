<?php

	/**
	 * Extends Log service implementing commands for XBMC start/stop. 
	 * 
	 * Starting and stopping XBMC requires root privileges. So, you 
	 * should use `sudo` before commands specified in service configuration.
	 * To allow www-data to run `sudo` for such commands, you should edit
	 * the `/etc/sudoers` file using `sudo visudo`. You can get info here:
	 *  - https://help.ubuntu.com/community/Sudoers
	 * 
	 * Configuration:
	 *  - file: XBMC log file full path, same as "file" for Log services
	 *  - startcommand: command that starts XBMC
	 *  - stopcommand: command that stops XBMC
	 *  - statuscommand: command that prints the status of XBMC (must have same output as "initctl status xbmc")
	 *  - [all other Log options]
	 * 
	 * Commands:
	 *  - start: starts XBMC
	 *  - stop: stops XBMC
	 * 
	 * Response:
	 *  - status: Status of XBMC (as given by the "statuscommand" specified in configuration)
	 *  - [all other Log responses]
	 */
	class XBMC extends Log {
		
		protected $startcommand;
		protected $stopcommand;
		protected $statuscommand;
		
		public function __construct($configuration) {
			
			// XBMC commands
			$this->startcommand = Service::config($configuration['startcommand']);
			$this->stopcommand = Service::config($configuration['stopcommand']);
			$this->statuscommand = Service::config($configuration['statuscommand']);
			
			// Log configuration
			parent::__construct($configuration);
			
		}
		
		public function run() {
			
			// Commands
			if ($_REQUEST['command'] == 'start') {
				LinuxUtils::exec($this->startcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'stop') {
				LinuxUtils::exec($this->stopcommand);
				$_REQUEST['command'] = null;
			}
			
			// Get XBMC status
			$status = LinuxUtils::exec($this->statuscommand);
			$GLOBALS['response']['status'] = trim($status);
			
			// Run Log service
			parent::run();
			
		}
		
	}
	
?>
