<?php

	/**
	 * Extends Ls service implementing commands for aMule controlling. 
	 * 
	 * Starting and stopping aMule requires root privileges. So, you 
	 * should use `sudo` before commands specified in service configuration.
	 * To allow www-data to run `sudo` for such commands, you should edit
	 * the `/etc/sudoers` file using `sudo visudo`. You can get info here:
	 *  - https://help.ubuntu.com/community/Sudoers
	 * 
	 * Configuration:
	 *  - folder: name of the aMule Incoming folder, same as "folder" for Ls services
	 *  - startcommand: command that starts aMule deamon
	 *  - stopcommand: command that stops aMule deamon
	 *  - startwebcommand: command that starts aMule web interface
	 *  - stopwebcommand: command that stops aMule web interface
	 *  - password: aMule password, used for `amulecmd`
	 *  - weburl: aMule web interface URL (you can use "[host]" placeholder, default "http://[host]:4711/")
	 *  - [all other Ls options]
	 * 
	 * Commands:
	 *  - start: starts aMule deamon
	 *  - stop: stops aMule deamon
	 *  - startweb: starts aMule web interface
	 *  - stopweb: stops aMule web interface
	 *  - [all other Ls commands]
	 * 
	 * Response:
	 *  - amuled: Number of `amuled` processes running (should be 1 if running or 0 if not, if 2 or more there's something wrong)
	 *  - amuleweb: Number of `amuleweb` processes running (should be 1 if running or 0 if not, if 2 or more there's something wrong)
	 *  - upspeed: Uploads speed, given only if aMule deamon is running
	 *  - downspeed: Downloads speed, given only if aMule deamon is running
	 *  - ed2k: ED2K connection status , given only if aMule deamon is running
	 *  - kad: KAS connection status, given only if aMule deamon is running
	 *  - weburl: aMule web interface URL
	 *  - [all other Ls responses, relative to the completed files in aMule Incoming folder]
	 */
	class aMule extends Ls {
		
		protected $startcommand;
		protected $stopcommand;
		protected $startwebcommand;
		protected $stopwebcommand;
		protected $password;
		protected $weburl = 'http://[host]:4711/';
		
		public function __construct($configuration) {
			
			// aMule commands
			$this->startcommand = Service::config($configuration['startcommand']);
			$this->stopcommand = Service::config($configuration['stopcommand']);
			$this->startwebcommand = Service::config($configuration['startwebcommand']);
			$this->stopwebcommand = Service::config($configuration['stopwebcommand']);
			
			// aMule password and web URL
			$this->password = Service::config($configuration['password']);
			$this->weburl = Service::config($configuration['weburl'], 'http://[host]:4711/');
			$this->weburl = str_replace('[host]', $_SERVER['HTTP_HOST'], $this->weburl);
			
			// Ls configuration
			parent::__construct($configuration);
			
		}
		
		public function run() {
			
			// Commands
			if ($_REQUEST['command'] == 'start') {
				LinuxUtils::exec($this->startcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'stop') {
				LinuxUtils::exec($this->stopcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'startweb') {
				LinuxUtils::exec($this->startwebcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'stopweb') {
				LinuxUtils::exec($this->stopwebcommand);
				$_REQUEST['command'] = null;
			}
			
			// Get aMule processes count
			$processes = LinuxUtils::exec('ps -e');
			$GLOBALS['response']['amuled'] = count(LinuxUtils::grep($processes, '/amuled/'));
			$GLOBALS['response']['amuleweb'] = count(LinuxUtils::grep($processes, '/amuleweb/'));
			
			// Get aMule status
			if ($GLOBALS['response']['amuled'] > 0) {
				$status = LinuxUtils::exec('amulecmd -c status -P', $this->password);
				foreach (array(
					'upspeed' => 'Upload',
					'downspeed' => 'Download',
					'ed2k' => 'eD2k',
					'kad' => 'Kad'
				) as $name => $label) {
					if (preg_match('/^\\h+>\\h+' . preg_quote($label, '/') . ':\\h+(.+)$/m', $status, $matches)) {
						$GLOBALS['response'][$name] = trim($matches[1]);
					}
				}
			}
			
			// Tell aMule web URL
			$GLOBALS['response']['weburl'] = $this->weburl;
			
			// Run Ls service
			parent::run();
			
		}
		
	}
	
	class aMuleException extends LsException { }
	
?>
