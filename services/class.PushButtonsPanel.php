<?php

	/**
	 * Executes named commands defined in configuratio. 
	 * 
	 * Clients should draw a collection of buttons for each command, and
	 * send the command name to execute when user clicks it.
	 * 
	 * If you need `sudo` for some commands (i.e. "sudo reboot") you should edit
	 * the `/etc/sudoers` file using `sudo visudo`. You can get info here:
	 *  - https://help.ubuntu.com/community/Sudoers
	 * 
	 * Configuration:
	 *  - columns: number of columns the client should use to draw buttons (default: 2)
	 *  - buttons: An array of objects, one for each command
	 *     - name: command identifier
	 *     - label: label for the button
	 *     - color: button color
	 *     - confirm: TRUE if the client should ask for confirmation
	 *     - command: command to execute
	 * 
	 * Commands:
	 *   - Any of the commands identifiers
	 * 
	 * Response:
	 *  - columns: number of columns the client should use to draw buttons
	 *  - buttons: the array of buttons (name, label, color, confirm)
	 *  - output: in response of a command execution, the whole stdout output
	 *  - retval: in response of a command execution, the return value (0 is success)
	 */
	class PushButtonsPanel extends Service {
		
		protected $columns = 2;
		protected $buttons = array();
		
		public function __construct($configuration) {
			$this->columns = max(1, intval(Service::config($configuration['columns'], 2)));
			$this->buttons = Service::config($configuration['buttons'], array()); 
		}
		
		public function run() {
			
			// Execute command?
			if (!empty($_REQUEST['command'])) {
				foreach ($this->buttons as $button) {
					if ($button['name'] == $_REQUEST['command']) {
						$retVal = 1;
						$GLOBALS['response']['output'] = trim(LinuxUtils::execRetVal($button['command'], $retVal));
						$GLOBALS['response']['retval'] = intval($retVal);
						$_REQUEST['command'] = null;
						break;
					}
				}

				// Command not found
				if (!empty($_REQUEST['command'])) throw new ServiceException('Unknown command "' . $_REQUEST['command'] . '"'); 

			}
			
			// Echo configuration, omitting buttons' commands
			$GLOBALS['response']['columns'] = $this->columns;
			$GLOBALS['response']['buttons'] = array_map(function($button) {
				unset($button['command']);
				return $button;
			}, $this->buttons);
			
		}
		
	}
	
?>
