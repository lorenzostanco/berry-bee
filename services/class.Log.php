<?php

	/**
	 * Returns last lines from a text file
	 * 
	 * Configuration:
	 *  - file: log file full path
	 *  - lines: number of lines to read (default: 100)
	 * 
	 * Response:
	 *  - path: log file complete path
	 *  - lines: an array of string, ending with last line found in the log file
	 */
	class Log extends Service {
		
		protected $file;
		protected $lines = 100;
		
		public function __construct($configuration) {
			$this->file = Service::config($configuration['file']);
			if (!file_exists($this->file)) throw new ServiceConfigurationException('Log file does not exist or is not readable.');
			if (!is_readable($this->file)) throw new ServiceConfigurationException('Log file is not readable.');
			$this->lines = intval(Service::config($configuration['lines'], 100));
		}
		
		public function run() {
			
			// Returns complete log file path
			$GLOBALS['response']['path'] = $this->file;
			
			// Use tail to get log lines
			$tail = trim(LinuxUtils::exec('tail -' . $this->lines, $this->file));
			
			// Split lines
			$GLOBALS['response']['lines'] = array();
			if (!empty($tail)) foreach (explode("\n", $tail) as $line) {
				$GLOBALS['response']['lines'][] = trim($line, "\r\n");
			}
			
		}
		
	}
	
?>
