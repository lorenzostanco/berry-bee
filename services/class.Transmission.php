<?php

	/**
	 * Extends Ls service implementing commands for Transmission deamon controlling. 
	 * 
	 * Starting and stopping Transmission requires root privileges. So, you 
	 * should use `sudo` before commands specified in service configuration.
	 * To allow www-data to run `sudo` for such commands, you should edit
	 * the `/etc/sudoers` file using `sudo visudo`. You can get info here:
	 *  - https://help.ubuntu.com/community/Sudoers
	 * 
	 * Configuration:
	 *  - folder: name of the torrents folder, same as "folder" for Ls services
	 *  - startcommand: command that starts Transmission deamon
	 *  - stopcommand: command that stops Transmission deamon
	 *  - rpcport: Transmission RPC port, default 9091
	 *  - rpcusername: Transmission RPC username, used for auth by `transmission-remote`
	 *  - rpcpassword: Transmission RPC password, used for auth by `transmission-remote`
	 *  - [all other Ls options]
	 * 
	 * Commands:
	 *  - start: starts Transmission deamon
	 *  - stop: stops Transmission deamon
	 *  - [all other Ls commands]
	 * 
	 * Response:
	 *  - running: TRUE if Transmission deamon is running, FALSE if not
	 *  - upspeed: Uploads speed, given only if Transmission deamon is running
	 *  - downspeed: Downloads speed, given only if Transmission deamon is running
	 *  - torrentscount: Total number of torrents (uploading, download, paused, all of them), given only if Transmission deamon is running
	 *  - weburl: Transmission web interface URL (i.e "http://[host]:9091/")
	 *  - [all other Ls responses, relative to the files in the torrents folder]
	 */
	class Transmission extends Ls {
		
		protected $startcommand;
		protected $stopcommand;
		protected $rpcport = 9091;
		protected $rpcusername;
		protected $rpcpassword;
		
		public function __construct($configuration) {
			
			// Transmission commands
			$this->startcommand = Service::config($configuration['startcommand']);
			$this->stopcommand = Service::config($configuration['stopcommand']);
			
			// Transmission auth and web URL
			$this->rpcport = intval(Service::config($configuration['rpcport'], 9091));
			$this->rpcusername = Service::config($configuration['rpcusername']);
			$this->rpcpassword = Service::config($configuration['rpcpassword']);
			
			// Ls configuration
			parent::__construct($configuration);
			
		}
		
		public function run() {
			
			// Commands
			if ($_REQUEST['command'] == 'start') {
				LinuxUtils::exec($this->startcommand);
				$_REQUEST['command'] = null;
			} elseif ($_REQUEST['command'] == 'stop') {
				LinuxUtils::exec($this->stopcommand);
				$_REQUEST['command'] = null;
			}
			
			// Get Transimission daemon running status (simply via ps)
			$processes = LinuxUtils::exec('ps -e');
			$GLOBALS['response']['running'] = count(LinuxUtils::grep($processes, '/transmission-d/')) > 0;
			
			// Get Transimission status
			if ($GLOBALS['response']['running']) {
				$list = LinuxUtils::exec('transmission-remote', 'localhost:' . $this->rpcport, '-n', $this->rpcusername . ':' . $this->rpcpassword, '-l');
				$list = trim($list);
				$list = empty($list) ? array() : explode("\n", $list);
				$GLOBALS['response']['torrentscount'] = max(0, count($list) - 2);
				$listSum = array();
				preg_match_all('/[0-9.\\-]+/', $list[count($list) - 1], $listSum);
				$list[count($list) - 1];
				$GLOBALS['response']['upspeed'] = $listSum[0][1];
				$GLOBALS['response']['downspeed'] = $listSum[0][2];
			}
			
			// Tell Transmission web URL
			$GLOBALS['response']['weburl'] = 'http://' . $_SERVER['HTTP_HOST'] . ':' . $this->rpcport . '/';
			
			// Run Ls service
			parent::run();
			
		}
		
	}
	
	class TransmissionException extends LsException { }
	
?>
