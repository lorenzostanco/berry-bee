**Berry bee** is an extensible web service written in PHP for Raspberry Pi 
(or any other Linux boxes) remote controlling.


Setup
-----

Berry bee requires an *Apache* web server with *PHP 5.3*:

	sudo apt-get install apache2-mpm-prefork php5

Make sure you have the `AllowOverride All` directive in your Apache `Directory` configuration.

Download this repository in a web-accessible folder (for example `/var/www/berry-bee`).
Set folders `auth` and `cache` as writeable and create `configuration.neon` (copying `configuration-sample.neon`).

The configuration file is in *NEON* format. It's quite intuitive, 
but if you have any doubt read [here](http://ne-on.org/).

Protect the `.git` folder from web accesses with:

	cp -v auth/.htaccess .git/

If you have problems with accented file names, make sure to tell Apache to use the 
system default locale. You should decomment the line `. /etc/default/locale` in
`/etc/apache2/envvars` and restart Apache.


Authentication
--------------

Client authentication is based on a secret token. Any unknown client that try to
connect to Berry bee must provide a device description and the secret passphrase 
defined in the ***auth*** configuration section. If the passphrase is correct, 
Berry bee generates and outputs a random token string to the client, and creates 
a file in the `auth` folder using that token as filename.

From now on, the client will pass that token on every service request to identify
itself to the server. Basically authentication occurs one time only, using 
passphrase to retrieve a valid token from the server.

This is an example of authentication process:

	/berry-bee/ # Auth token not given!
	/berry-bee/?service=auth&device=Droid&passphrase=wrong # Wrong authentication passphrase!
	/berry-bee/?service=auth&device=Droid&passphrase=correct # Token "ABC" is given to the client

At this point file `ABC.token` has been created in `auth` folder. This file 
contains device description and a requests log:

	Droid
	[2001-02-28 12:34:56] 123.45.67.89 /berry-bee/?token=ABC
	[2001-02-28 12:34:56] 123.45.67.89 /berry-bee/?token=ABC&service=status

Now *Droid* device is authenticated (forever, until `ABC.token` esists) and 
can run services:

	/berry-bee/?token=ABC&service=status&...

Because the token is specified in every request URL, is reccomended to run
Berry bee over HTTPS and not over HTTP.


Folders
-------

For security reasons, filesystem operation on Berry bee (as directory listing, 
file removing, USB flash managing) never explicitly specify full folders path. 
Folders are referenced using "names", and *name to path*  mappings are defined 
in the ***folders*** configuration section.

This way services cannot access and do stuffs in filesystem locations not listed here.


Services
--------

**Services** are declared in the ***services*** configuration section. 
Each service is an "instance" of a *service class*. Some service classes may 
require a configuration for the instance: in this case, configuration is given 
in the service declaration.

New **service classes** can be developed by writing classes in `services/` that 
extend the `Service` base class.

Services can parse **commands**. Commands are identified by a command name.
You can give zero or one command for each request URL. The following example
runs command *bar* of service *foo*:

	/berry-bee/?token=ABC&service=foo&command=bar

Commands can accept named parameters. The request URL in the next example runs
command *bar* of service *foo* giving parameters *number* and *text*:

	/berry-bee/?token=ABC&service=foo&command=bar&number=12&text=Lorem%20Ipsum


License
-------

Berry bee is released under the terms of the **GNU General Public License (GPL)** 
version 3. Full text license is in `LICENSE.txt`. 

*NEON* configuration file parser is part of the [Nette Framework](http://nette.org) and
it's released under the terms of either the New BSD License or the GNU General Public 
License (GPL) version 2 or 3.
